# Laravel Gdpr

[![Build Status](https://travis-ci.org/cherrypulp/laravel-gdpr.svg?branch=master)](https://travis-ci.org/cherrypulp/laravel-gdpr)
[![Packagist](https://img.shields.io/packagist/l/cherrypulp/laravel-gdpr.svg)](https://packagist.org/packages/cherrypulp/laravel-gdpr)

Laravel GDPR integration based on vue-gdpr

## Installation

Install via composer
```bash
composer require cherrypulp/laravel-gdpr
```

Install the vue-gdpr :

```bash
npm install @cherrypulp/vue-gdpr --registry http://npm.cherrypulp.com
```

### Register Service Provider

**Note! This and next step are optional if you use laravel>=5.5 with package
auto discovery feature.**

Add service provider to `config/app.php` in `providers` section

```php
Cherrypulp\LaravelGdpr\ServiceProvider::class,
```

### Register Facade

Register package facade in `config/app.php` in `aliases` section

```php
Cherrypulp\LaravelGdpr\Facades\LaravelGdpr::class,
```

### Publish Configuration File

```bash
php artisan vendor:publish --provider="Cherrypulp\LaravelGdpr\ServiceProvider" --tag="config"
```

## Usage

With your other scripts add :

```php
@include('gdpr::script')
```

*In your main VueJS bootstrap* : 

```VueJS
import VueGdprPlugin from '@cherrypulp/vue-gdpr';
Vue.use(VueGdprPlugin);
```


* Inside your VueJS app * :

Include the gdpr bar at a top:

```php
@include('gdpr::bar')
```

Include the gdpr modal, in the bottom :

```php
@include('gdpr::modal')
```

Exemple : 

```php
    <div class="app" role="main" v-cloak>
        @include('gdpr::bar')
        <header class="app-header">
            @yield('header')
        </header>

        <main class="app-body">
            @yield('content')
        </main>

        <footer class="app-footer" role="contentinfo">
            @section('footer')
                @yield('footer')
                {{-- @include('parts.footer-simple') --}}
            @show
        </footer>

        @stack('modals')
        @include('gdpr::modal')
    </div>

    # Don't put it outside your Vue App !
```

## Retrieve cookies informations

When accepted => you will have a cookie called gdpr_levels

```php
$_COOKIE('gdpr_levels')

//or

Cookie::get('gdpr_levels')

//or

request()->cookie('gdpr_levels')

```

## Configure the package

All the configuration is handled by the config and lang file and rely on the vue-gdpr package.

For more information on available :

https://gitlab.com/cherrypulp/components/vue-gdpr

## Security

If you discover any security related issues, please email
instead of using the issue tracker.

## Credits

- [Daniel Sum](https://gitlab.com/cherrypulp/components/laravel-gdpr)
- [All contributors](https://gitlab.com/cherrypulp/components/laravel-gdpr/graphs/contributors)
