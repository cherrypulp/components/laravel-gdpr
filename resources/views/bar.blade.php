<gdpr-bar
    button-agreement="{{ __('gdpr.accept') }}"
    button-preferences="{{ __('gdpr.settings') }}"
    content="{{ __('gdpr.content') }}"
></gdpr-bar>
