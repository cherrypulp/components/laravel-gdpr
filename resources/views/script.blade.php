@php
$levels = __(config('gdpr.langref').'.levels');

if (is_array($levels)) {
    $levels = json_encode($levels);
}
@endphp
<script type="application/json" id="{{ config('gdpr.id') }}">{!! $levels !!}</script>
