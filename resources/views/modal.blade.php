<gdpr-modal
    button-agreement="{{ __('gdpr.agree') }}"
    button-close="{{ __('gdpr.modal.close') }}"
    content="{{ __('gdpr.modal.content') }}"
    label-active="{{ __('gdpr.enabled') }}"
    label-inactive="{{ __('gdpr.disabled') }}"
    title="{{ __('gdpr.modal.title') }}"
></gdpr-modal>
