<?php

return [
    'content' => 'We use cookies to give you the best online experience. Some of them cannot be deactivated. By using our website, you agree to our use of cookies in accordance with our cookie policy.',
    "settings" => 'Settings',
    'accept' => 'Accept',
    'modal' => [
        'title' => 'Cookie settings'
    ],
    'agree' => 'I agree',
    'enabled' => 'Enabled',
    'disabled' => 'Disabled',
    'levels' => [
        'required' => [
            'title' => 'Strictly necessary cookies',
            'content' => 'These cookies are essential for the operation of the site.',
            'scripts' => [
                [
                    'position' => 'head',
                    'content' => '',
                ],
                [
                    'position' => 'body',
                    'content' => '',
                ],
                [
                    'position' => 'footer',
                    'content' => '',
                ],
            ],
        ],
        'third_party' => [
            'title' => '3rd party cookies',
            'content' => 'Keeping this cookie enabled helps us to improve our website.',
            'scripts' => [
                [
                    'position' => 'head',
                    'content' => '',
                ],
                [
                    'position' => 'body',
                    'content' => '',
                ],
                [
                    'position' => 'footer',
                    'content' => '',
                ],
            ],
        ],
    ]
];
