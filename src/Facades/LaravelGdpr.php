<?php

namespace Cherrypulp\LaravelGdpr\Facades;

use Illuminate\Support\Facades\Facade;

class LaravelGdpr extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'laravel-gdpr';
    }
}
