<?php

namespace Cherrypulp\LaravelGdpr;

class LaravelGdpr
{
    public static function alreadyConsentedWithCookies()
    {
        return \Cookie::has(config('cookie-consent.cookie_name'));
    }
}
