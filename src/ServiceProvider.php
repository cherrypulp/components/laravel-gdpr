<?php

namespace Cherrypulp\LaravelGdpr;

use Cookie;
use View;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    const CONFIG_PATH = __DIR__ . '/../config/gdpr.php';

    public function boot()
    {
        $this->publishes([
            self::CONFIG_PATH => config_path('gdpr.php'),
        ], 'config');

        $this->publishes([
            __DIR__ . '/../resources/views' => base_path('resources/views/vendor/gdpr'),
        ], 'views');

        $this->publishes([
            __DIR__ . '/../resources/lang' => base_path('resources/lang/vendor/gdpr'),
        ], 'lang');

        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'gdpr');

        $this->mergeConfigFrom(__DIR__ . '/../config/gdpr.php', 'gdpr');

        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'gdpr');
    }

    public function register()
    {
        $this->mergeConfigFrom(self::CONFIG_PATH, 'gdpr');

        $this->app->bind('laravel-gdpr', function () {
            return new LaravelGdpr();
        });
    }
}
