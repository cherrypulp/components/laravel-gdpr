<?php

namespace Cherrypulp\LaravelGdpr\Tests;

use Cherrypulp\LaravelGdpr\Facades\LaravelGdpr;
use Cherrypulp\LaravelGdpr\ServiceProvider;
use Orchestra\Testbench\TestCase;

class LaravelGdprTest extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'laravel-gdpr' => LaravelGdpr::class,
        ];
    }

    public function testExample()
    {
        $this->assertEquals(1, 1);
    }
}
